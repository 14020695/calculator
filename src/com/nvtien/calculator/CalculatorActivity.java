package com.nvtien.calculator;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by Vance on 06/11/2015.
 */
public class CalculatorActivity extends Activity implements View.OnClickListener {

    private TextView tvLastQuestion, tvThisQuestion;
    private Calculation myCalculation;
    private Button[] numBt;
    private Button btPoint, btPlus, btMinus, btMultiplication, btDivision, btEqual, btNegative, btPercent;
    private Button btBackspace, btCE;
    private boolean pointClicked, justFinishedCalculation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.calculator);
        pointClicked=false;
        justFinishedCalculation=false;
        initButtons();
        initTextView();
    }

    private void initTextView() {
        /**Mapped the text views here with the text views in layout*/
        tvLastQuestion = (TextView)findViewById(R.id.tv_last_question);
        tvThisQuestion = (TextView)findViewById(R.id.tv_this_question);
        myCalculation= new Calculation();
        tvThisQuestion.setText(myCalculation.getThisQuestion());
        tvLastQuestion.setText(myCalculation.getLastQuestion());
    }

    private void initButtons() {
        /**Mapped the buttons here with the buttons in layout*/
        numBt= new Button[10];
        numBt[0]=(Button)findViewById(R.id.bt_0);
        numBt[1]=(Button)findViewById(R.id.bt_1);
        numBt[2]=(Button)findViewById(R.id.bt_2);
        numBt[3]=(Button)findViewById(R.id.bt_3);
        numBt[4]=(Button)findViewById(R.id.bt_4);
        numBt[5]=(Button)findViewById(R.id.bt_5);
        numBt[6]=(Button)findViewById(R.id.bt_6);
        numBt[7]=(Button)findViewById(R.id.bt_7);
        numBt[8]=(Button)findViewById(R.id.bt_8);
        numBt[9]=(Button)findViewById(R.id.bt_9);
        btPoint=(Button)findViewById(R.id.bt_point);
        btPlus=(Button)findViewById(R.id.bt_plus);
        btMinus=(Button)findViewById(R.id.bt_minus);
        btMultiplication=(Button)findViewById(R.id.bt_multiplication);
        btDivision=(Button)findViewById(R.id.bt_division);
        btEqual=(Button)findViewById(R.id.bt_equal);
        btBackspace=(Button)findViewById(R.id.bt_backspace);
        btCE=(Button)findViewById(R.id.bt_ce);
        btNegative=(Button)findViewById(R.id.bt_negative);
        btPercent=(Button)findViewById(R.id.bt_percent);

        /**Set onClickListener to all buttons*/
        for(int i=0; i<10; i++){
            numBt[i].setOnClickListener(this);
        }
        btPoint.setOnClickListener(this);
        btPlus.setOnClickListener(this);
        btMinus.setOnClickListener(this);
        btMultiplication.setOnClickListener(this);
        btDivision.setOnClickListener(this);
        btEqual.setOnClickListener(this);
        btBackspace.setOnClickListener(this);
        btCE.setOnClickListener(this);
        btNegative.setOnClickListener(this);
        btPercent.setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onClick(View v) {
        /**Input a number*/
        for(int i=0; i<10;i++){
            if(v==numBt[i]){
                if(justFinishedCalculation){
                    myCalculation.clearEverything();
                    tvThisQuestion.setText("");
                    justFinishedCalculation=false;
                }
                if(myCalculation.getThisQuestion().endsWith("%")){
                    return;
                }
                addThisQuestion(""+i);
            }
        }

        /**Input a point to the number*/
        if(v==btPoint){
            if(justFinishedCalculation){
                myCalculation.clearEverything();
                tvThisQuestion.setText("");
                justFinishedCalculation=false;
            }
            if(pointClicked){
                return;
            }
            addThisQuestion(".");
            pointClicked=true;
        }

        /**Input the negative symbol to a number*/
        if(v==btNegative){
            myCalculation.addNegative();
            tvThisQuestion.setText(myCalculation.getThisQuestion());
        }

        /**Input the percentage symbol*/
        if(v==btPercent){
            pointClicked=false;
            if(myCalculation.getThisQuestion().endsWith("+")||myCalculation.getThisQuestion().endsWith("-")||
                    myCalculation.getThisQuestion().endsWith("x")||myCalculation.getThisQuestion().endsWith("/")||
                    myCalculation.getThisQuestion().endsWith("%")) {
                return;
            }
            addThisQuestion("%");
        }

        /**Input the plus/minus/multiplication/division symbol*/
        if(v==btPlus||v==btMinus||v==btMultiplication||v==btDivision){
            justFinishedCalculation=false;
            if(myCalculation.getThisQuestion().isEmpty()){
                return;
            }
            if(myCalculation.getThisQuestion().endsWith("+")||myCalculation.getThisQuestion().endsWith("-")||
                    myCalculation.getThisQuestion().endsWith("x")|| myCalculation.getThisQuestion().endsWith("/")) {
                myCalculation.backspace();
            }
            pointClicked=false;
            Button thisButton =(Button)v;
            addThisQuestion(thisButton.getText().toString());
        }

        /**If you want to delete the latest character of the question*/
        if(v==btBackspace){
            myCalculation.backspace();
            tvThisQuestion.setText(myCalculation.getThisQuestion());
        }

        /**If you want to delete every thing in the question*/
        if(v==btCE){
            myCalculation.clearEverything();
            tvThisQuestion.setText("");
        }

        /**After you done with input, calculate*/
        if(v==btEqual){
            if(myCalculation.getThisQuestion().isEmpty()){
                return;
            }
            if(myCalculation.getThisQuestion().endsWith("+")||myCalculation.getThisQuestion().endsWith("-")||
                    myCalculation.getThisQuestion().endsWith("x")||myCalculation.getThisQuestion().endsWith("/")) {
                return;
            }
            pointClicked=false;
            myCalculation.calculate();
            tvThisQuestion.setText(myCalculation.getThisQuestion());
            tvLastQuestion.setText(myCalculation.getLastQuestion());
            justFinishedCalculation=true;
        }
    }

    /**Add some text to the question*/
    private void addThisQuestion(String s){
        myCalculation.append(s);
        tvThisQuestion.setText(myCalculation.getThisQuestion());
    }

}
