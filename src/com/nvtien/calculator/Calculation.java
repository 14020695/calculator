package com.nvtien.calculator;

import android.util.Log;

/**
 * Created by Vance on 06/11/2015.
 */
public class Calculation {
    private String thisQuestion;
    private float lastQuestion;
    private String num;

    public Calculation(){
        lastQuestion=0;
        thisQuestion="";
    }

    public String getLastQuestion() {
        String answer=""+lastQuestion;
        lastQuestion=0;
        return answer;
    }

    public String getThisQuestion() {
        return thisQuestion;
    }

    /**Add some text to the question*/
    public void append(String s) {
        thisQuestion+=s;
    }

    /**Remove the latest character in the question*/
    public void backspace(){
        if(thisQuestion.isEmpty()){
            return;
        }
        thisQuestion=thisQuestion.substring(0,thisQuestion.length()-1);
    }

    /**Clear the question*/
    public void clearEverything(){
        thisQuestion="";
    }

    /**Do the calculating*/
    public void calculate() {
        standardise();
        /**Differ the multiply negative numbers calculation with the minus calculation*/
        while(thisQuestion.contains("x-")){
            thisQuestion=thisQuestion.replace("x-", "x_");
        }
        while(thisQuestion.contains("/-")){
            thisQuestion=thisQuestion.replace("/-", "/_");
        }
        /**Do the plus and minus first*/
        while (checkPM()){
            int plus = thisQuestion.indexOf("+");
            int minus = thisQuestion.indexOf("-");

            if(minus==0){
                minus=thisQuestion.indexOf("-",1);
            }
            num=setNum(plus,minus);
            num=multiplyNum(num);
            lastQuestion+=Float.parseFloat(num);
        }
        thisQuestion=multiplyNum(thisQuestion);
        lastQuestion+=Float.parseFloat(thisQuestion);
        thisQuestion=""+lastQuestion;
    }

    /**Standardise the calculation*/
    private void standardise() {
        while(thisQuestion.contains("--")){
            thisQuestion=thisQuestion.replace("--", "+");
        }

        while (thisQuestion.contains("%+")){
            thisQuestion=thisQuestion.replace("%+","/100+");
        }
        while (thisQuestion.contains("%-")){
            thisQuestion=thisQuestion.replace("%-","/100-");
        }
        while (thisQuestion.contains("%x")){
            thisQuestion=thisQuestion.replace("%x","/100x");
        }
        if(thisQuestion.endsWith("%")){
            thisQuestion=thisQuestion.substring(0,thisQuestion.length()-1)+"/100";
        }
    }

    /**Set the addend number to the calculation*/
    private String setNum(int plus, int minus){
        String num="";
        if(minus>plus && plus>0){
            /**If thisQuestion got both plus and minus sign and plus sign is the first*/
            num= thisQuestion.substring(0,plus);
            thisQuestion=thisQuestion.substring(plus+1);
        }else if(plus>minus && minus>0){
            /**If thisQuestion got both plus and minus sign and minus sign is the first*/
            num= thisQuestion.substring(0,minus);
            thisQuestion=thisQuestion.substring(minus);
        }else if(minus<=0){
            /**If thisQuestion only contains plus sign (the minus sign on the first of the string doesn't count)*/
            num= thisQuestion.substring(0,plus);
            thisQuestion=thisQuestion.substring(plus+1);
        }else if(plus<0){
            /**If thisQuestion only contains minus sign*/
            num= thisQuestion.substring(0,minus);
            thisQuestion=thisQuestion.substring(minus);
        }
        return num;
    }

    /**Do the multiply and divide*/
    private String multiplyNum(String num) {
        /**Change the minus sign back*/
        while(num.contains("_")){
            num=num.replace("_", "-");
        }
        float answer = 1;
        boolean mOrP = true;
        String s1;

        while (checkMD(num)) {
            int multiply = num.indexOf("x");
            int divide = num.indexOf("/");
            if (multiply < 0) {
                /**If num contains only divide sign*/
                s1 = num.substring(0, divide);
                if (mOrP) {
                    answer *= Float.parseFloat(s1);
                } else {
                    answer /= Float.parseFloat(s1);
                }
                num = num.substring(divide + 1);
                mOrP = false;
            } else if (divide < 0) {
                /**If num contains only multiply sign*/
                s1 = num.substring(0, multiply);
                if (mOrP) {
                    answer *= Float.parseFloat(s1);
                } else {
                    answer /= Float.parseFloat(s1);
                }
                num = num.substring(multiply + 1);
                mOrP = true;
            } else {
                if(multiply>divide){
                    /**If the divide is the first sign on num*/
                    s1=num.substring(0,divide);
                    if (mOrP) {
                        answer *= Float.parseFloat(s1);
                    } else {
                        answer /= Float.parseFloat(s1);
                    }
                    num = num.substring(divide + 1);
                    mOrP=false;

                } else{
                    /**If the multiply is the first sign on num*/
                    s1=num.substring(0,multiply);
                    if (mOrP) {
                        answer *= Float.parseFloat(s1);
                    } else {
                        answer /= Float.parseFloat(s1);
                    }
                    num = num.substring(multiply + 1);
                    mOrP=true;
                }
            }
        }
        /**If num contains no multiply or divide sign anymore*/
        if (mOrP) {
            answer *= Float.parseFloat(num);
        } else {
            answer /= Float.parseFloat(num);
        }
        num = "" + answer;
        return num;
    }

    /**Check if we still need to do plus or minus*/
    private boolean checkPM(){
        if(thisQuestion.contains("+")){
            return true;
        }
        if(thisQuestion.lastIndexOf("-")>0){
            return true;
        }
        return false;
    }

    /**Check if we still need to do multiply or divide*/
    private boolean checkMD(String num){
        if(num.contains("x")){
            return true;
        }
        if(num.contains("/")){
            return true;
        }
        return false;
    }

    /**Add a negative sign to a number*/
    public void addNegative(){
        boolean negative = false;
        int[] sign = new int[4];
        sign[0] = thisQuestion.lastIndexOf("+");
        sign[1] = thisQuestion.lastIndexOf("-");
        sign[2] = thisQuestion.lastIndexOf("x");
        sign[3] = thisQuestion.lastIndexOf("/");
        int max=-1;
        for(int i=0; i<4; i++){
            if(max<sign[i]){
                max=sign[i];
            }
        }
        /**If thisQuestion contains only numbers*/
        if(max==-1){
            thisQuestion="-"+thisQuestion;
            return;
        }

        /**If thisQuestion contains only a negative symbol*/
        if(max==sign[1]&&thisQuestion.length()==1){
            thisQuestion="";
        }
        /**If the latest character of thisQuestion is a calculation symbol*/
        if(max==thisQuestion.length()-1){
            return;
        }

        /**If thisQuestion contains only a negative symbol, remove it*/
        if(max==sign[1]&&max==0){
            thisQuestion=thisQuestion.substring(1);
            return;
        }

        /**If the number already got a negative symbol, remove it*/
        if(max==sign[1]){
            if(thisQuestion.charAt(max-1)=='+'||thisQuestion.charAt(max-1)=='-'||
                    thisQuestion.charAt(max-1)=='x'||thisQuestion.charAt(max-1)=='/'){
                negative=true;
            }
        }

        /**Add or remove a negative symbol*/
        if(negative){
            String s1 = thisQuestion.substring(0,max);
            String s2 = thisQuestion.substring(max);
            thisQuestion=s1+s2.substring(1);
        } else {
            String s1 = thisQuestion.substring(0,max+1);
            String s2 = thisQuestion.substring(max+1);
            thisQuestion=s1+"-"+s2;
        }
    }
}
